Projeto 4

O projeto 4 consiste no estudo e experimentação da arquitetura P2P, utilizando uma das tecnologias:

- WebRTC
- STUN
- Socket.io P2P
- BitTorrent
- BitCoin / Blockchain
- ipfs

A tecnologia escolhida para este trabalho foi o BitCoin/Blockchain

Aluno:

    - Newton Junior Ferreira Silva


O projeto tem uma estrutura de documentacao de explicacao, um manual para desenvolvedor e um manual para clientes.
Todos eles presentes na Wiki desse projeto
